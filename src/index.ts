import P5 from 'p5';
import 'p5/lib/addons/p5.sound';
import song from './assets/notorious.mp3';

const setupCanvas = (p5: P5) => {
	const canvas = p5.createCanvas(800, 800);
	canvas.parent("sketch");
	p5.background(0);
	p5.textSize(20);
	p5.stroke(255);
  p5.noFill();
	p5.colorMode(p5.HSB, 360, 150, 100, 1);
}

const drawKaleidoscope = (p5: P5, nLines: number) => {
	p5.strokeWeight(15);
	const v1: number = p5.map(p5.mouseX, 0, p5.width, 0, 255);
	const v2: number = p5.map(p5.mouseX, 0, p5.width, 100, 200);
	const v3: number = p5.map(p5.mouseY, 0, p5.height, 100, 255);

	p5.stroke(v1, v2, v3, 0.5);
	p5.background(0);

	for (let i = 0; i < (nLines); i++) {
		p5.rotate(p5.TWO_PI/nLines);
		const x0: number = p5.mouseX-p5.width/2;
		const y0: number = p5.mouseY-p5.height/2;
		const x1: number = p5.pmouseX-p5.width/2;
		const y1: number = p5.pmouseY-p5.height/2;
		const tf: number = 0.5;
		p5.push();
		p5.line(x0, y0, x1, y1);
		p5.scale(-1, 1);
		p5.line(-x0, -y0, -x1, -y1);
		p5.circle(x0/2, y0/2, 10);
		p5.pop();
	}
}

const kaleidoscope = (p5: P5) => {
	
	const nLines = 3;
	let mic: any;
	let soundFile: P5.SoundFile
	let recorder: P5.SoundRecorder;
	let playButton: P5.Element | null;

	p5.preload = () => {
		soundFile = new P5.SoundFile(song);
		console.log('soundFile = ', soundFile);
	};

	p5.setup = () => {
		setupCanvas(p5);
		playButton =  p5.select('#play-button');
		console.log('button: ',playButton);

		// recorder = new P5.SoundRecorder();
		// console.log('recorder: ', recorder);
		//soundFile.play();
	};

	p5.draw = () => {
		p5.translate(p5.width/2, p5.height/2);


	};

};

new P5(kaleidoscope);



